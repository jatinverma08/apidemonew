//
//  ViewController.swift
//  secondNew
//
//  Created by jatin verma on 2019-10-16.
//  Copyright © 2019 jatin verma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    @IBOutlet weak var result: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func button(_ sender: Any) {
        print("button pressed")
        
        let websiteURL = "https://api.sunrise-sunset.org/json?lat=43.6532&lng=-79.3832"
        AF.request(websiteURL).responseJSON{
        (xyz) in
            print(xyz.value)
            
            let x = JSON(xyz.value)
            let sunrise = x["results"]["sunrise"]
            let sunset = x["results"]["sunset"]
            
            print(sunset)
            print(sunrise)
            self.result.text = "Sunrise: \(sunrise)"
        }
        
    }
    
}

