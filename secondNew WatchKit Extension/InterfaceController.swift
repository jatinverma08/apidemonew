//
//  InterfaceController.swift
//  secondNew WatchKit Extension
//
//  Created by jatin verma on 2019-10-16.
//  Copyright © 2019 jatin verma. All rights reserved.
//

import WatchKit
import Foundation
import SwiftyJSON
import Alamofire

class InterfaceController: WKInterfaceController {

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    @IBOutlet weak var response: WKInterfaceLabel!
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    @IBAction func output() {
        let websiteURL = "https://api.sunrise-sunset.org/json?lat=43.6532&lng=-79.3832"
        AF.request(websiteURL).responseJSON{
        (xyz) in
            print(xyz.value)
            
            let x = JSON(xyz.value)
            let sunrise = x["results"]["sunrise"]
            let sunset = x["results"]["sunset"]
            
            print(sunset)
            print(sunrise)
            self.response.setText("Sunrise: \(sunrise)") 
        }
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
